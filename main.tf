####################################################
#? General
####################################################
data "azurerm_client_config" "current" {}
resource "azurerm_resource_group" "main" {
  name     = var.resource_group_name
  location = var.location
}

####################################################
#? Virtual Network
####################################################
resource "azurerm_virtual_network" "main" {
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  name          = var.virtual_network_name
  address_space = var.virtual_network_address_space
}
resource "azurerm_subnet" "main" {
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name

  name             = var.subnet_name
  address_prefixes = var.subnet_address_prefixes
}
####################################################
#? Azure Kubernetes Service
####################################################
resource "azurerm_kubernetes_cluster" "main" {
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  name               = var.kubernetes_name
  dns_prefix         = var.kubernetes_dns_prefix
  kubernetes_version = var.kubernetes_version

  service_principal {
    client_id     = var.kubernetes_sp_client_id
    client_secret = var.kubernetes_sp_client_secret
  }

  default_node_pool {
    name       = var.kubernetes_node_name
    node_count = var.kubernetes_node_count
    vm_size    = var.kubernetes_node_vm_size
  }
}

####################################################
#? Azure Key Vault
####################################################
resource "azurerm_key_vault" "main" {
  tenant_id           = data.azurerm_client_config.current.tenant_id
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  name                       = var.kv_name
  soft_delete_retention_days = var.kv_soft_delete_retention_days
  sku_name                   = var.kv_sku_name
}

# ####################################################
# #? Azure Postgresql
# ####################################################
resource "azurerm_postgresql_flexible_server" "main" {
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location

  name = var.psql_name

  administrator_login    = var.psql_administrator_user
  administrator_password = var.psql_administrator_password

  sku_name              = var.psql_sku_name
  version               = var.psql_version
  storage_mb            = var.psql_storage_mb
  backup_retention_days = var.psql_backup_retention_days
}