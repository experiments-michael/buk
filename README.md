## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.1.5 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >= 2.95.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | >= 2.95.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_key_vault.main](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault) | resource |
| [azurerm_kubernetes_cluster.main](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/kubernetes_cluster) | resource |
| [azurerm_postgresql_flexible_server.main](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/postgresql_flexible_server) | resource |
| [azurerm_resource_group.main](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group) | resource |
| [azurerm_subnet.main](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet) | resource |
| [azurerm_virtual_network.main](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_network) | resource |
| [azurerm_client_config.current](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/client_config) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_kubernetes_dns_prefix"></a> [kubernetes\_dns\_prefix](#input\_kubernetes\_dns\_prefix) | DNS prefix specified when creating the managed cluster | `string` | n/a | yes |
| <a name="input_kubernetes_name"></a> [kubernetes\_name](#input\_kubernetes\_name) | The name of the Managed Kubernetes Cluster to create. Changing this forces a new resource to be created. | `string` | n/a | yes |
| <a name="input_kubernetes_node_count"></a> [kubernetes\_node\_count](#input\_kubernetes\_node\_count) | The initial number of nodes which should exist in this Node Pool. | `number` | `1` | no |
| <a name="input_kubernetes_node_name"></a> [kubernetes\_node\_name](#input\_kubernetes\_node\_name) | The name which should be used for the default Kubernetes Node Pool. Changing this forces a new resource to be created. | `string` | n/a | yes |
| <a name="input_kubernetes_node_vm_size"></a> [kubernetes\_node\_vm\_size](#input\_kubernetes\_node\_vm\_size) | The size of the Virtual Machine for the Kubernetes Node Pool | `string` | n/a | yes |
| <a name="input_kubernetes_sp_client_id"></a> [kubernetes\_sp\_client\_id](#input\_kubernetes\_sp\_client\_id) | The Client ID for the Service Principal. | `string` | n/a | yes |
| <a name="input_kubernetes_sp_client_secret"></a> [kubernetes\_sp\_client\_secret](#input\_kubernetes\_sp\_client\_secret) | The Client Secret for the Service Principal. | `string` | n/a | yes |
| <a name="input_kubernetes_version"></a> [kubernetes\_version](#input\_kubernetes\_version) | Version of Kubernetes specified when creating the AKS managed cluster. | `string` | n/a | yes |
| <a name="input_kv_name"></a> [kv\_name](#input\_kv\_name) | Specifies the name of the Key Vault. | `string` | n/a | yes |
| <a name="input_kv_sku_name"></a> [kv\_sku\_name](#input\_kv\_sku\_name) | The Name of the SKU used for this Key Vault. | `string` | `"standard"` | no |
| <a name="input_kv_soft_delete_retention_days"></a> [kv\_soft\_delete\_retention\_days](#input\_kv\_soft\_delete\_retention\_days) | The number of days that items should be retained for once soft-deleted. | `number` | `7` | no |
| <a name="input_location"></a> [location](#input\_location) | The Azure region where the resources should exist. | `string` | n/a | yes |
| <a name="input_psql_administrator_password"></a> [psql\_administrator\_password](#input\_psql\_administrator\_password) | The Password associated with the 'postgresql\_administrator\_user' for the PostgreSQL Server | `string` | n/a | yes |
| <a name="input_psql_administrator_user"></a> [psql\_administrator\_user](#input\_psql\_administrator\_user) | The Administrator Login for the PostgreSQL Server. | `string` | n/a | yes |
| <a name="input_psql_backup_retention_days"></a> [psql\_backup\_retention\_days](#input\_psql\_backup\_retention\_days) | Backup retention days for the server, supported values are between 7 and 35 days. | `number` | `7` | no |
| <a name="input_psql_name"></a> [psql\_name](#input\_psql\_name) | Specifies the name of the PostgreSQL Server. Changing this forces a new resource to be created. | `string` | n/a | yes |
| <a name="input_psql_sku_name"></a> [psql\_sku\_name](#input\_psql\_sku\_name) | Specifies the SKU Name for this PostgreSQL Server. | `string` | n/a | yes |
| <a name="input_psql_storage_mb"></a> [psql\_storage\_mb](#input\_psql\_storage\_mb) | Max storage allowed for a server. | `number` | `32768` | no |
| <a name="input_psql_version"></a> [psql\_version](#input\_psql\_version) | Specifies the version of PostgreSQL to use. | `string` | `"13"` | no |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | The Name which should be used for this Resource Group. Changing this forces a new Resource Group to be created. | `string` | n/a | yes |
| <a name="input_subnet_address_prefixes"></a> [subnet\_address\_prefixes](#input\_subnet\_address\_prefixes) | The address prefixes to use for the subnet. | `list(string)` | n/a | yes |
| <a name="input_subnet_name"></a> [subnet\_name](#input\_subnet\_name) | The name of the subnet. Changing this forces a new resource to be created. | `string` | n/a | yes |
| <a name="input_virtual_network_address_space"></a> [virtual\_network\_address\_space](#input\_virtual\_network\_address\_space) | The address space that is used the virtual network. You can supply more than one address space. | `list(string)` | n/a | yes |
| <a name="input_virtual_network_name"></a> [virtual\_network\_name](#input\_virtual\_network\_name) | The name of the virtual network. Changing this forces a new resource to be created. | `string` | n/a | yes |

## Outputs

No outputs.